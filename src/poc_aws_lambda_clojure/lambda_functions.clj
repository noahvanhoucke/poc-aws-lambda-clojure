(ns poc-aws-lambda-clojure.lambda-functions
  (:gen-class
    :methods [[ping [String com.amazonaws.services.lambda.runtime.Context] String]]))

(defn -ping
  [this input context]
  (str "pong - " input))